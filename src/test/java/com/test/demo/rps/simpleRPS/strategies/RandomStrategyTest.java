package com.test.demo.rps.simpleRPS.strategies;

import java.util.Arrays;
import java.util.List;

import com.demo.rps.simpleRPS.enumerate.Choice;
import com.demo.rps.simpleRPS.strategies.impl.RandomStrategy;


public class RandomStrategyTest extends StrategyTest {

	private List<Choice> values = Arrays.asList(Choice.values());
	
	public RandomStrategyTest(String testName) {
		super(testName);
	}
	
	@Override
	protected RandomStrategy makeContractSubject() {
		return new RandomStrategy();
	}
	
	public void testRandomInRange(){
		assertTrue(values.contains(iStrategy.genStrategy()));
	}

}
