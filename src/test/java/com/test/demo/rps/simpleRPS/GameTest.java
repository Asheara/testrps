package com.test.demo.rps.simpleRPS;


import com.demo.rps.simpleRPS.enumerate.Result;
import com.demo.rps.simpleRPS.game.Game;

import junit.framework.TestCase;

public class GameTest extends TestCase {
	
	private Game game;
	
	public GameTest(String testName) {
		super(testName);
	}

    public void testNewGame()
    {
    	game = new Game();
        assertNotNull(game);
        assertNotNull(game.getResults());
        assertTrue(game.getResults().isEmpty());
    }
    
    public void testPlay(){
    	game = new Game();
    	Result res = game.play();
    	assertTrue(res instanceof Result);
    	assertNotNull(res);
    }
    
    public void testNotNull(){
    	game = new Game();
    	assertNotNull(game.getResults());
    }
	
}
