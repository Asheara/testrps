package com.test.demo.rps.simpleRPS.strategies;

import org.junit.Before;

import com.demo.rps.simpleRPS.strategies.IStrategy;

import junit.framework.TestCase;

public abstract class StrategyTest extends TestCase {

	public IStrategy iStrategy;

    protected abstract IStrategy makeContractSubject();
    
    public StrategyTest(String testName) {
		super(testName);
	}

    @Before
    public void setUp() {
    	iStrategy = makeContractSubject();
    }

}
