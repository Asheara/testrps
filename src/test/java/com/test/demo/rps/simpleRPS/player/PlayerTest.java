package com.test.demo.rps.simpleRPS.player;


import com.demo.rps.simpleRPS.player.Player;
import com.demo.rps.simpleRPS.strategies.impl.RandomStrategy;
import com.demo.rps.simpleRPS.strategies.impl.RockStrategy;

import junit.framework.TestCase;

public class PlayerTest extends TestCase{
	
	public Player player;
	
	public PlayerTest(String testName) {
		super(testName);
	}
	
	public void testRandomPlayer(){
		player = new Player();
		assertTrue(player.getStrategy() instanceof RandomStrategy);	
	}
	
	public void testRockPlayer(){
		player = new Player();
		RockStrategy rs = new RockStrategy();
		player.setStrategy(rs);
		assertTrue(player.getStrategy() instanceof RockStrategy);
	}
}
