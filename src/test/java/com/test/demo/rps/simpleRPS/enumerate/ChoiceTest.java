package com.test.demo.rps.simpleRPS.enumerate;

import junit.framework.TestCase;
import com.demo.rps.simpleRPS.enumerate.*;

public class ChoiceTest extends TestCase {

	public ChoiceTest(String testName) {
		super(testName);
	}

	public void testP1Wins() {
		assertEquals(Choice.evaluate(Choice.ROCK, Choice.SCISSORS), Result.P1_WINS);
		assertEquals(Choice.evaluate(Choice.SCISSORS, Choice.PAPER), Result.P1_WINS);
		assertEquals(Choice.evaluate(Choice.PAPER, Choice.ROCK), Result.P1_WINS);
	}

	public void testP2Wins() {
		assertEquals(Choice.evaluate(Choice.ROCK, Choice.PAPER), Result.P2_WINS);
		assertEquals(Choice.evaluate(Choice.SCISSORS, Choice.ROCK), Result.P2_WINS);
		assertEquals(Choice.evaluate(Choice.PAPER, Choice.SCISSORS), Result.P2_WINS);
	}

	public void testDraws() {
		assertEquals(Choice.evaluate(Choice.ROCK, Choice.ROCK), Result.DRAW);
		assertEquals(Choice.evaluate(Choice.SCISSORS, Choice.SCISSORS), Result.DRAW);
		assertEquals(Choice.evaluate(Choice.PAPER, Choice.PAPER), Result.DRAW);
	}

	public void testValues() {
		assertEquals(0, Choice.ROCK.getChoiceValue());
		assertEquals(1, Choice.PAPER.getChoiceValue());
		assertEquals(2, Choice.SCISSORS.getChoiceValue());
	}
}
