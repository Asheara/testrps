package com.test.demo.rps.simpleRPS.strategies;

import com.demo.rps.simpleRPS.enumerate.Choice;
import com.demo.rps.simpleRPS.strategies.impl.RockStrategy;

public class RockStrategyTest extends StrategyTest {

	public RockStrategyTest(String testName) {
		super(testName);
	}
	
	@Override
	protected RockStrategy makeContractSubject() {
		return new RockStrategy();
	}
	
	public void testAlwaysRock(){
		assertSame(Choice.ROCK, iStrategy.genStrategy());
	}

}
