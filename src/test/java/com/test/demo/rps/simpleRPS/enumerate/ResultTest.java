package com.test.demo.rps.simpleRPS.enumerate;

import com.demo.rps.simpleRPS.enumerate.Result;

import junit.framework.TestCase;

public class ResultTest extends TestCase {
	
	public ResultTest(String testName) {
		super(testName);
	}
	
	public void testValues() {
		assertEquals(0, Result.DRAW.getValue());
		assertEquals(1, Result.P1_WINS.getValue());
		assertEquals(-1, Result.P2_WINS.getValue());
	}

}
