package com.demo.rps.simpleRPS.enumerate;

/**
 * @author Raquel
 * Class to define possible results
 */
public enum Result {

	DRAW(0), P1_WINS(1), P2_WINS(-1);

	private final int value;

	Result(final int newValue) {
		value = newValue;
	}

	public int getValue() {
		return value;
	}

}
