package com.demo.rps.simpleRPS.enumerate;


/**
 * @author Raquel
 * Class to define possible choices a player can take
 */
public enum Choice {
	
	ROCK{
		@Override
		public Choice getLoser() {
			return Choice.valueOf("PAPER");
		}
	}, PAPER {
		@Override
		public Choice getLoser() {
			// TODO Auto-generated method stub
			return Choice.valueOf("SCISSORS");
		}
	}, SCISSORS {
		@Override
		public Choice getLoser() {
			// TODO Auto-generated method stub
			return Choice.valueOf("ROCK");
		}
	};

	public boolean loseAgainst(Choice otherChoice){
		return otherChoice.equals(this.getLoser());	
	}
	
	public abstract Choice getLoser();
	
	public int getChoiceValue(){
		return ordinal();
	}
	
	public static Choice getChoiceByVal( int val ) {
		for ( Choice choice: values()) {
			if ( choice.getChoiceValue() == val ) {
				return choice;
			}
		}
		return Choice.ROCK;
	}
	
	
	/*
	 * *
	 * Rock beats scissors
	 * Paper beats rock
	 * Scissors beats paper
	 */
	public static Result evaluate(Choice player1, Choice player2){
		if(player1.loseAgainst(player2)){
			return Result.P2_WINS;
		} else if(player2.loseAgainst(player1)){
			return Result.P1_WINS;
		}else{
			return Result.DRAW;
		}
	}

}
