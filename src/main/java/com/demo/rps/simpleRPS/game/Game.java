package com.demo.rps.simpleRPS.game;

import java.util.ArrayList;
import java.util.List;

import com.demo.rps.simpleRPS.player.Player;
import com.demo.rps.simpleRPS.strategies.impl.RockStrategy;
import com.demo.rps.simpleRPS.enumerate.*;

/**
 * @author Raquel 
 * Class to implement logic executed when the game is played
 *         (Players take choices, evaluate results of those choices)
 */
public class Game {

	private List<String> results = new ArrayList<String>();

	private Player rockPlayer = new Player();
	private Player randomPlayer = new Player();

	public Game() {
		super();
	}

	public Result play() {
		rockPlayer.setStrategy(new RockStrategy());
		Result rs = Choice.evaluate(rockPlayer.doChoice(), randomPlayer.doChoice());
		return rs;
	}

	public List<String> getResults() {
		return results;
	}

	public void setResults(List<String> results) {
		this.results = results;
	}

}
