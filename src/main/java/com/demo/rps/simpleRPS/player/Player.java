package com.demo.rps.simpleRPS.player;

import com.demo.rps.simpleRPS.enumerate.Choice;
import com.demo.rps.simpleRPS.strategies.IStrategy;
import com.demo.rps.simpleRPS.strategies.impl.RandomStrategy;

/**
 * @author Raquel
 *
 * Class to define player behavior, by default random strategy. Player behavior will depends on strategy set.
 */
public class Player {

	private IStrategy strategy;
	
	public Player(){
		strategy = new RandomStrategy();
	}
	
	public Choice doChoice(){
		return strategy.genStrategy();
	}

	public IStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(IStrategy strategy) {
		this.strategy = strategy;
	}


	
}
