package com.demo.rps.simpleRPS.strategies.impl;

import com.demo.rps.simpleRPS.enumerate.Choice;
import com.demo.rps.simpleRPS.strategies.IStrategy;


/**
 * @author Raquel
 *
 * Class to implement a random choice strategy.
 */
public class RandomStrategy implements IStrategy {

	public Choice genStrategy() {
		int ch = (int) (Math.random() * 3);
		return Choice.getChoiceByVal(ch);
	}

}
