package com.demo.rps.simpleRPS.strategies;

import com.demo.rps.simpleRPS.enumerate.Choice;

/**
 * @author Raquel
 * 
 * Interface to define basic strategy
 */
public interface IStrategy {

	Choice genStrategy();
}
