package com.demo.rps.simpleRPS.strategies.impl;

import com.demo.rps.simpleRPS.enumerate.Choice;
import com.demo.rps.simpleRPS.strategies.IStrategy;


/**
 * @author Raquel
 *
 * Class to implement fixed rock strategy.
 */
public class RockStrategy implements IStrategy {

	public Choice genStrategy() {
		return Choice.ROCK;
	}

}
