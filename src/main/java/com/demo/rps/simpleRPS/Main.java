package com.demo.rps.simpleRPS;

import com.demo.rps.simpleRPS.enumerate.Result;
import com.demo.rps.simpleRPS.game.Game;

/**
 * @author Raquel
 * Launcher class for the game
 *
 */
public class Main 
{
    public static void main( String[] args )
    {
    	Game game = new Game();
		for (int i = 0; i < 100; i++) {
			Result rs = game.play();
			game.getResults().add("Round "+(i+1)+" results in "+rs.name());
		}
		for(String result : game.getResults()){
			System.out.println(result);
		}   
    }
}
